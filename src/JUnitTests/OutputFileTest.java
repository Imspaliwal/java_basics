package JUnitTests;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OutputFileTest {
	
/*	
 * @Author: Sumit Paliwal
 * This program will define the textable features of JUnit test. */

	
/*	The code is not readable
	The code is not easy to maintain.
	When the test suite is complex the code could contain logical issues.*/
	
/*	private File output;
	output=new File(...);
	output.delete();

	public void testFile1() {
		// Code to verify Test Case 1
	}
	output.delete();
	output=new File(...);

	public void testFile2() {
		// Code to verify Test Case 2
	}
	output.delete();*/
	
	
	
    private File output; 
    @Before 
    public void createOutputFile() 
    { 
       output = new File(...);
    }
  
	@After 
	public void deleteOutputFile() 
    {
        output.delete(); 
    } 
     
    @Test 
    public void testFile1() 
    {
       // code for test case objective
    } 
	@Test 
	public void testFile2() 
    {
       // code for test case objective
    }

}
