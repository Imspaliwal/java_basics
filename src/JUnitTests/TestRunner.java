package JUnitTests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	
	public static void main(String[] args) {
		
		Result result = JUnitCore.runClasses(TestJunit.class);
		
		int totalTestRunCount = result.getRunCount();
		long runTime = result.getRunTime();
		int faliureCount = result.getFailureCount();
		int ignoreCount = result.getIgnoreCount();
		
		System.out.println("Total Test Run Count: "+totalTestRunCount);
		System.out.println("Total Run Time: "+runTime);
		System.out.println("Faliure Test Count: "+faliureCount);
		System.out.println("Ignore Test Count: "+ignoreCount);
		
		for(Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println(result.wasSuccessful());
	}

}
