package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import AllString.Anagram2;

/*JUnit test to test various anagram program to various string input.
 * 
 * @author = Sumit Paliwal
 * */

public class JUnitAnagram2Test {

	@Test
	public void testIsAnagram() {
		
		assertTrue(Anagram2.isAnagram("word", "wrdo"));
//		assertTrue(Anagram2.isAnagram("ccc", "ccccc"));
		assertTrue("Not an anagram", Anagram2.isAnagram("Sleep", "slep"));
		
	}
	
	
}
