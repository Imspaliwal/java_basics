package AllCollection.Basic;

import java.util.Hashtable;
import java.util.Vector;

public class CollectionNeed {

	/*	Need for Collection Framework : 
		Before Collection Framework (or before JDK 1.2) was introduced, 
		the standard methods for grouping Java objects (or collections) were Arrays or Vectors or Hashtables. 
		All of these collections had no common interface.
		Accessing elements of these Data Structures was a hassle as each had a different method (and syntax) 
		for accessing its members:*/

		public static void main(String[] args) {

		int arr[] = new int[] {1, 2, 3, 4};
		Vector<Integer> v = new Vector();
		Hashtable<Integer, String> h = new Hashtable();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		h.put(1, "Hi");
		h.put(2, "WHY WE NEED Collection");
		h.put(3, "Bye");

		System.out.println(arr[1]);
		System.out.println(v.elementAt(1));
		System.out.println(h.get(2));

	}

}
