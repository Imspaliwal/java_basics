package AllCollection.Basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class CollectionsDisjointDemo {

	public static void main(String[] args) {

		//		Create different collection having common (or not common) elements in it.

		List<String> myList1 = new ArrayList<String>();	
		myList1.add("Practice");
		myList1.add("Code");
		myList1.add("Bravo");
		myList1.add("Practice");

		List<String> myList2 = new Vector<String>();
		myList2.add("Disjoint");
		myList2.add("Check");
		myList2.add("Done");

		List myList3 = new Vector();
		myList3.add(1);
		myList3.add("Practice");

		Set<String> myList4 = new HashSet<String>();
		myList4.add("Practice");
		myList4.add("Code");
		myList4.add("Bravo");
		myList4.add("Practice");

		// Here we are using disjoint() method to check  
		// whether two collections are disjoint or not, return true if dont have any common element. 

		System.out.println(Collections.disjoint(myList1, myList2)); //true
		System.out.println(Collections.disjoint(myList1, myList3));	//flase
		System.out.println(Collections.disjoint(myList1, myList4));	//flase

		/*	Arrays class in Java doesn�t have disjoint method. 
			We can use Collections.disjoint() to check quickly disjoincy of two arrays.*/


		System.out.println(Collections.frequency(myList1, "Practice"));

		/*	How to Quickly get frequency of an element in an array in Java ?

			Arrays class in Java doesn�t have frequency method. 
			But we can use Collections.frequency() to get frequency of an element in an array also.*/
		
		Collections.rotate(myList1, 2);
		System.out.println(myList1);

	}

}
