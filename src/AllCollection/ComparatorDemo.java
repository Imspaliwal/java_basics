package AllCollection;

import java.util.Arrays;
import java.util.Comparator;

class Person implements Comparable {
  private String firstName;

  private String lastName;

  private int age;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int compareTo(Object anotherPerson) throws ClassCastException {
    if (!(anotherPerson instanceof Person)) {
      throw new ClassCastException("A Person object expected.");
    }
    int anotherPersonAge = ((Person) anotherPerson).getAge();
    return this.age - anotherPersonAge;
  }
}

class LastNameComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    String lastName1 = ((Person) person).getLastName().toUpperCase();
    String firstName1 = ((Person) person).getFirstName().toUpperCase();
    String lastName2 = ((Person) anotherPerson).getLastName().toUpperCase();
    String firstName2 = ((Person) anotherPerson).getFirstName().toUpperCase();
    
    if (lastName1.equals(lastName2)) {
      return firstName1.compareTo(firstName2);
    } else {
      return lastName1.compareTo(lastName2);
    }
  }
}

class FirstNameComparator implements Comparator {
  public int compare(Object person, Object anotherPerson) {
    String lastName1 = ((Person) person).getLastName().toUpperCase();
    String firstName1 = ((Person) person).getFirstName().toUpperCase();
    String lastName2 = ((Person) anotherPerson).getLastName().toUpperCase();
    String firstName2 = ((Person) anotherPerson).getFirstName().toUpperCase();
    if (firstName1.equals(firstName2)) {
      return lastName1.compareTo(lastName2);
    } else {
      return firstName1.compareTo(firstName2);
    }
  }
}

public class ComparatorDemo {
  public static void main(String[] args) {
    Person[] persons = new Person[4];
    persons[0] = new Person();
    persons[0].setFirstName("A");
    persons[0].setLastName("X");
    persons[0].setAge(56);

    persons[1] = new Person();
    persons[1].setFirstName("S");

    persons[1].setLastName("C");
    persons[1].setAge(8);

    persons[2] = new Person();
    persons[2].setFirstName("E");
    persons[2].setLastName("H");
    persons[2].setAge(16);

    persons[3] = new Person();
    persons[3].setFirstName("B");
    persons[3].setLastName("Q");
    persons[3].setAge(69);

    System.out.println("Natural Order");
    for (int i = 0; i < 4; i++) {
      Person person = persons[i];
      String lastName = person.getLastName();
      String firstName = person.getFirstName();
      int age = person.getAge();
      System.out.println(lastName + ", " + firstName + ". Age:" + age);
    }

    Arrays.sort(persons, new LastNameComparator());
    System.out.println();
    System.out.println("Sorted by last name");
    for (int i = 0; i < 4; i++) {
      Person person = persons[i];
      String lastName = person.getLastName();
      String firstName = person.getFirstName();
      int age = person.getAge();
      System.out.println(lastName + ", " + firstName + ". Age:" + age);
    }
    Arrays.sort(persons, new FirstNameComparator());
    System.out.println();
    System.out.println("Sorted by first name");
    for (int i = 0; i < 4; i++) {
      Person person = persons[i];
      String lastName = person.getLastName();
      String firstName = person.getFirstName();
      int age = person.getAge();
      System.out.println(lastName + ", " + firstName + ". Age:" + age);
    }

    Arrays.sort(persons);
    System.out.println();
    System.out.println("Sorted by age");
    for (int i = 0; i < 4; i++) {
      Person person = persons[i];
      String lastName = person.getLastName();
      String firstName = person.getFirstName();
      int age = person.getAge();
      System.out.println(lastName + ", " + firstName + ". Age:" + age);
    }
  }
}
