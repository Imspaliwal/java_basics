package AllString;

import java.util.Scanner;

public class Palindrome1 {
	
	public static String reverse(String str) {
		
		if(str == null || str.isEmpty()){
			return str;
		}
		
		return str.charAt(str.length() - 1) + reverse(str.substring(0, str.length() - 1));
	}
	
	public static Boolean isPalindrome(String str) {
		
		if(reverse(str).equals(str)) {
			return true;
		}else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String: ");
		String input = sc.next();
		sc.close();
		System.out.println("Reverse of the String is: "+reverse(input));
		System.out.println("String is Palindrome: "+isPalindrome(input));
	}

}
