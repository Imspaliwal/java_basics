package AllString;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram1 {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		String str1 = sc.next();
		String str2 = sc.next();
		sc.close();
		System.out.println(isAnagram(str1, str2));
		
	}
		public static boolean isAnagram(String s1, String s2){
			char[] word1 = s1.toLowerCase().replaceAll("\\s", "").toCharArray();
			char[] word2 = s2.toLowerCase().replaceAll("\\s", "").toCharArray();
			Arrays.sort(word1);
			Arrays.sort(word2);
			
			return Arrays.equals(word1, word2);
		}
	

}
