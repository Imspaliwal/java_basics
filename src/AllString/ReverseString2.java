package AllString;

import java.util.Scanner;

public class ReverseString2 {
	
/*	Reverse Sting with Recursion
 * 
 * @author = Sumit 
 * */
	
	public static String reverse(String input) {
		
		if(input == null || input.isEmpty()) {
			return input;
		}
		
		return input.charAt(input.length() - 1) + reverse(input.substring(0, input.length() - 1));
	}
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		sc.close();
		System.out.println(reverse(str));
	}

}
