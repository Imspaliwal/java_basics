package AllString;

import java.util.Scanner;

public class Palindrome2 {
	
	public static String reverse(String str) {
		
		if(str == null || str.isEmpty()) {
			
			return str;
		}
		
		char[] c = str.toCharArray();
		String strReverse = "";
		
		for(int i = str.length() - 1; i >= 0; i--) {
			
			strReverse = strReverse + c[i];
		}
		
		return strReverse;
	}
	
	public static Boolean isPalindrome(String str) {
		
		if(reverse(str).equals(str)) {
			return true;
		}
		
		return false;
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String: ");
		String input = sc.nextLine();
		sc.close();
		System.out.println("Reverse of the String: "+reverse(input));
		System.out.println("Palindrome of String: "+isPalindrome(input));
	}

}
