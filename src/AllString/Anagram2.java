package AllString;

import java.util.Scanner;

/*Anagram Program
 * This Program check of two string are anagram or not
 * @author Sumit Paliwal
*/
public class Anagram2 {
	
	public static boolean isAnagram(String word, String anagram) {
		
		if(word.length() != anagram.length()) {
			return false;
		}
		
		char[] chars = word.toLowerCase().toCharArray();
		
		for(char c : chars) {
			int index = anagram.indexOf(c);
			if(index != -1) {
				anagram = anagram.substring(0, index) + anagram.substring(index + 1);
			}else {
				return false;
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String str1 = sc.next();
		String str2 = sc.next();
		sc.close();
		Boolean result = isAnagram(str1, str2);
		
		System.out.println(result);
	}

}
