package AllString;

import java.util.Arrays;
import java.util.Scanner;

public class Duplicate {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String: ");
		String input = sc.next();
		sc.close();
		Arrays.sort(input.toCharArray());
		System.out.println(removeDupsSorted(input));
	}
		
	    public static String removeDupsSorted(String str) 
	    { 
	        int res_ind = 1, ip_ind = 1; 
	          
	        // Character array for removal of duplicate characters 
	        char arr[] = str.toCharArray(); 
	          
	        /* In place removal of duplicate characters*/
	        while (ip_ind != arr.length) 
	        { 
	            if(arr[ip_ind] != arr[ip_ind-1]) 
	            { 
	                arr[res_ind] = arr[ip_ind]; 
	                res_ind++; 
	            } 
	            ip_ind++; 
	            
	        } 
	      
	        str = new String(arr); 
	        return str.substring(0,res_ind); 
	    } 
}

