package Increment_decrement;

class A 
{
	public static void main(String[] args) 
	{
	  	int i = 0;
		System.out.println(i++);		// k = 0 ,i = 1, value of i is use to next statement 
		System.out.println(i);			// i = 1
	}
}
