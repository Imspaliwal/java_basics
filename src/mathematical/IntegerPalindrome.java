package mathematical;

import java.util.Scanner;

public class IntegerPalindrome {
	
	public static void main(String [] args) {
		
		Scanner sc = new Scanner(System.in);
		int input = sc.nextInt();
		sc.close();
		int reverse = 0;
		
		while(input!=0){
			int rem = input % 10;
			reverse = reverse * 10 + rem;
			input = input / 10;
			
		}
		System.out.println(reverse);
	}

}
